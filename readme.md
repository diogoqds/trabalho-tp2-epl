# Trabalho de TP2

## Alunos
- Diogo Queiroz dos Santos - 15/0123418
- Vitor Ribeiro Custódio - 15/0023502

Desenvolvemos o projeto em Scala usando mixins e traits, usando como base a implementação disponibilizada pelo professor.

E usando Ruby foi desenvolvida a mesma aplicação, usando o padrão de projeto Visitor, foram implementados testes usando a biblioteca RSpec.

## Rodando os testes

#### Scala

- Para rodar os testes do projeto em Scala, use o comando: **sbt test** 

#### Ruby

- Para rodar os testes em Ruby, primeiro certifique-se que tenha o Ruby instalado.
- Use o comando **bundle install** para instalar a gema rspec (a lib para testes do ruby).
- E para executar os testes basta executar o comando **rspec**.