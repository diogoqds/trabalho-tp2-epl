package br.unb.cic.epl

import org.scalatest.FlatSpec
import org.scalatest.Matchers
import org.scalatest.GivenWhenThen
import org.scalatest.BeforeAndAfter


class TestExpression extends FlatSpec with Matchers with GivenWhenThen with BeforeAndAfter {

  behavior of "An Add expression"

  var literal100: Eval.Literal = _
  var literal200: Eval.Literal = _
  var literal300: Eval.Literal = _
  var literalN10: Eval.Literal = _

  var lit100: Height.Literal = _
  var lit200: Height.Literal = _
  var lit300: Height.Literal = _
  var litN10: Height.Literal = _

  before {

    literal100 = new Core.Literal(100) with Eval.Literal
    literal200 = new Core.Literal(200) with Eval.Literal
    literal300 = new Core.Literal(300) with Eval.Literal
    literalN10 = new Core.Literal(-10) with Eval.Literal
    
    lit100 = new Core.Literal(100) with Height.Literal
    lit200 = new Core.Literal(200) with Height.Literal
    lit300 = new Core.Literal(300) with Height.Literal
    litN10 = new Core.Literal(-10) with Height.Literal
    
  }

    it should "return String ((100 + 200) - (300 * -10))" in { 
        val add = new AddEval.Add(literal100, literal200)
        val mult = new MultEval.Mult(literal300, literalN10)
        val sub = new SubEval.Sub(add,mult)
        
        sub.print should be("((100 + 200) - (300 * -10))")
    }

    it should "return 300 when we call sub.eval" in {  

        val add = new AddEval.Add(literal100, literal200)
        val mult = new MultEval.Mult(literal300, literalN10)
        val sub = new SubEval.Sub(add,mult)

        sub.eval() should be(3300)
    }

    it should "return 3 when we call height.result" in {                                                                                                           
        
        val add = new AddHeight.Add(lit100, lit200)
        val mult = new MultHeight.Mult(lit300, litN10)
        val sub = new SubHeight.Sub(add,mult)
        
        sub.height() should be(3)
    }

}
