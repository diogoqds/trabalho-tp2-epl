require 'visitor'

class Height
    include Visitor

    def visit(expression) 
        @result = 0
        if expression.instance_of?(Core::Literal)
            @result = 1
        else
            heightLeft = expression.left.accept(self)
            heightRight = expression.right.accept(self)
            if heightLeft > heightRight
                @result = heightLeft + 1
            else
                @result = heightRight + 1
            end
        end
    end

    def result
        @result
    end
end
