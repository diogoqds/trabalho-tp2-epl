require 'core'
class Add 
    include Core::Expression
    
    attr_reader :left, :right
    
    def initialize(left,right)
        @left = left
        @right = right
    end

    def print
        if @left.instance_of?(Core::Literal) and @right.instance_of?(Core::Literal)
            "(#{@left.value} + #{@right.value})"
        else
            "(#{@left.print} + #{@right.print})"
        end
    end

    def accept(visitor)
        visitor.visit(self)
    end
end
