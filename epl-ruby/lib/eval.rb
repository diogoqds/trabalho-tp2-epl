require 'visitor'
require 'add'
require 'sub'
require 'core'

class Eval
    include Visitor

    attr_accessor :result
    attr_reader :left, :right

    def visit(expression)
        @result = 0
        if expression.instance_of?(Core::Literal)
            @result = expression.value
        elsif expression.instance_of?(Add)
            expression.left.accept(self)
            valueLeft = @result
            expression.right.accept(self)
            valueRight = @result
            @result = valueLeft + valueRight
        elsif expression.instance_of?(Sub)
            expression.left.accept(self)
            valueLeft = @result
            expression.right.accept(self)
            valueRight = @result
            @result = valueLeft - valueRight
        elsif expression.instance_of?(Mult)
            expression.left.accept(self)
            valueLeft = @result
            expression.right.accept(self)
            valueRight = @result
            @result = valueLeft * valueRight      
        end
    end

    def result
        @result
    end
end
