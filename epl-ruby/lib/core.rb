require 'visitor'

module Core
    module Expression
        include Visitor
        
        def print
        end

        def accept
        end
    end    

    class Literal
        include Expression
        
        attr_accessor :value
        
        def initialize(value)
            @value = value
        end

        def value
            @value.to_i
        end

        def print
            @value.to_s            
        end

        def accept(visitor)
            visitor.visit(self)
        end
    end
end
