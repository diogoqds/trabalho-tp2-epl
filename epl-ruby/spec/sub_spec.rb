require 'sub'
require 'core'
require 'eval'
require 'height'

describe Sub, "An Sub Expression" do
    
    before(:each) do
        @literal0 = Core::Literal.new(0)
        @literal100 = Core::Literal.new(100)
        @literal200 = Core::Literal.new(200)
        @literalN10 = Core::Literal.new(-10)
        @literal300 = Core::Literal.new(300)
        @literal400 = Core::Literal.new(400)
        @literal500 = Core::Literal.new(500)
        @literal600 = Core::Literal.new(600)
        @literal700 = Core::Literal.new(700)
        @literal800 = Core::Literal.new(800)
    end

    it "return String (100 - 200)" do 
        sub = Sub.new(@literal100, @literal200)
        
        expect(sub.print).to eq("(100 - 200)")
    end
    
    it "return String (0 - 200)" do 
        sub = Sub.new(@literal0, @literal200)
        
        expect(sub.print).to eq("(0 - 200)")
    end
    
    it "return 300 when we call eval.result" do  
        eval = Eval.new
        sub = Sub.new(@literal100, @literal200)

        sub.accept(eval)

        expect(eval.result).to eq(-100)
    end

    it 'return 10 when we call eval.result' do
        eval = Eval.new
        sub = Sub.new(@literal0,@literalN10)
        
        sub.accept(eval)
        expect(eval.result).to eq(10)
    end

    it "return 3 when we call height.result" do  
        height = Height.new                                                                                                           
        sub1 = Sub.new(@literal300,@literal400)
        sub2 = Sub.new(@literal500,@literal600)
        sub = Sub.new(sub1,sub2)

        sub.accept(height)
        expect(height.result).to eq(3)
    end

    it "return 5 when we call height.result" do   
        height = Height.new                                                                                                          
        sub1 = Sub.new(@literal300,@literal400)
        sub2 = Sub.new(@literal500,@literal600)
        sub3 = Sub.new(sub1,sub2)
        sub4 = Sub.new(sub3, @literal700)
        sub = Sub.new(sub4, @literal800)

        sub.accept(height)
        expect(height.result).to eq(5)
    end
end

