require 'add'
require 'core'
require 'eval'
require 'height'

describe Add, "An Add Expression" do
    
    before(:each) do
        @literal0 = Core::Literal.new(0)
        @literal100 = Core::Literal.new(100)
        @literal200 = Core::Literal.new(200)
        @literalN10 = Core::Literal.new(-10)
        @literal300 = Core::Literal.new(300)
        @literal400 = Core::Literal.new(400)
        @literal500 = Core::Literal.new(500)
        @literal600 = Core::Literal.new(600)
        @literal700 = Core::Literal.new(700)
        @literal800 = Core::Literal.new(800)
    end

    it "return String (100 + 200)" do 
        add = Add.new(@literal100, @literal200)
        
        expect(add.print).to eq("(100 + 200)")
    end
    
    it "return String (0 + 200)" do 
        add = Add.new(@literal0, @literal200)
        
        expect(add.print).to eq("(0 + 200)")
    end
    
    it "return 300 when we call eval.result" do  
        eval = Eval.new
        add = Add.new(@literal100, @literal200)

        add.accept(eval)

        expect(eval.result).to eq(300)
    end

    it 'return -10 when we call eval.result' do
        eval = Eval.new
        add = Add.new(@literal0,@literalN10)
        
        add.accept(eval)
        expect(eval.result).to eq(-10)
    end


    it "return 3 when we call height.result" do   
        height = Height.new                                                                                                          
        
        add1 = Add.new(@literal300,@literal400)
        add2 = Add.new(@literal500,@literal600)
        add = Add.new(add1,add2)
        
        add.accept(height)
        expect(height.result).to eq(3)
    end

    it "return 5 when we call height.result" do                                                                                                             
        height = Height.new                                                                                                          
        
        add1 = Add.new(@literal300,@literal400)
        add2 = Add.new(@literal500,@literal600)
        add3 = Add.new(add1,add2)
        add4 = Add.new(add3, @literal700)
        add = Add.new(add4, @literal800)
        
        add.accept(height)
        expect(height.result).to eq(5)
    end
end

