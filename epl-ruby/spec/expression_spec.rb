require 'add'
require 'sub'
require 'mult'
require 'core'
require 'eval'
require 'height'

describe "Expressions" do
    
    before(:each) do
        @literal0 = Core::Literal.new(0)
        @literal100 = Core::Literal.new(100)
        @literal200 = Core::Literal.new(200)
        @literalN10 = Core::Literal.new(-10)
        @literal300 = Core::Literal.new(300)
        @literal400 = Core::Literal.new(400)
        @literal500 = Core::Literal.new(500)
        @literal600 = Core::Literal.new(600)
        @literal700 = Core::Literal.new(700)
        @literal800 = Core::Literal.new(800)
    end

    it "return String ((100 + 200) - (300 * -10))" do 
        add = Add.new(@literal100, @literal200)
        mult = Mult.new(@literal300, @literalN10)
        sub = Sub.new(add,mult)
        
        expect(sub.print).to eq("((100 + 200) - (300 * -10))")
    end
    
    it "return 300 when we call eval.result" do  
        eval = Eval.new

        add = Add.new(@literal100, @literal200)
        mult = Mult.new(@literal300, @literalN10)
        sub = Sub.new(add,mult)
        

        sub.accept(eval)

        expect(eval.result).to eq(3300)
    end

    it "return 3 when we call height.result" do   
        height = Height.new                                                                                                          
        
        add = Add.new(@literal100, @literal200)
        mult = Mult.new(@literal300, @literalN10)
        sub = Sub.new(add,mult)
        
        sub.accept(height)
        expect(height.result).to eq(3)
    end
end

