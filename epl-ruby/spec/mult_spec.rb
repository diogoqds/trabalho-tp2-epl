require 'mult'
require 'core'
require 'eval'
require 'height'

describe Mult, "An Mult Expression" do
    
    before(:each) do
        @literal0 = Core::Literal.new(0)
        @literal100 = Core::Literal.new(100)
        @literal200 = Core::Literal.new(200)
        @literalN10 = Core::Literal.new(-10)
        @literal300 = Core::Literal.new(300)
        @literal400 = Core::Literal.new(400)
        @literal500 = Core::Literal.new(500)
        @literal600 = Core::Literal.new(600)
        @literal700 = Core::Literal.new(700)
        @literal800 = Core::Literal.new(800)
    end

    it "return String (100 * 200)" do 
        mult = Mult.new(@literal100, @literal200)
        
        expect(mult.print).to eq("(100 * 200)")
    end
        
    it "return String (0 * 200)" do 
        mult = Mult.new(@literal0, @literal200)
        
        expect(mult.print).to eq("(0 * 200)")
    end
    
    it "return 20000 when we call eval.result" do  
        eval = Eval.new
        mult = Mult.new(@literal100, @literal200)

        mult.accept(eval)

        expect(eval.result).to eq(20000)
    end

    it 'return 0 when we call eval.result' do
        eval = Eval.new
        mult = Mult.new(@literal0,@literalN10)
        
        mult.accept(eval)
        expect(eval.result).to eq(0)
    end

    it "return 3 when we call height.result" do    
        height = Height.new

        mult1 = Mult.new(@literal300,@literal400)
        mult2 = Mult.new(@literal500,@literal600)
        mult = Mult.new(mult1,mult2)
        
        mult.accept(height)
        expect(height.result).to eq(3)
    end

    it "return 5 when we call height.result" do 
        height = Height.new 

        mult1 = Mult.new(@literal300,@literal400)
        mult2 = Mult.new(@literal500,@literal600)
        mult3 = Mult.new(mult1,mult2)
        mult4 = Mult.new(mult3, @literal700)
        mult = Mult.new(mult4, @literal800)
        
        mult.accept(height)
        expect(height.result).to eq(5)
    end
end

